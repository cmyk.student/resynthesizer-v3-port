# Resynthesizer V3 Port (Work in Progress)

This is a work-in-progress repository to port the Resynthesizer plug-in from GIMP 2 to GIMP 3 APIs. Hopefully it will be incorporated in the main branch once all changes are made. 

Currently the Heal Selection, Heal Transparency, Map Style, Uncrop, Render Texture, Resynth Fill Pattern, Resynth Sharpen, and Resynth Enlarge plug-ins have been ported. Feedback is appreciated!

## Installation Guide

- Download the [deprecation branch](https://github.com/bootchk/resynthesizer/tree/deprecations) code of the Resynthesizer plug-in

- Download the code from this repository and extract it in the same directory. Overwrite all duplicate files.

- **Windows Users**: You may need to copy **config.h** from the GIMP source code into this directory for it to compile. This file is generated after you run the **configure.ac** file. You can find more information on the [GIMP developer site](https://developer.gimp.org/core/setup/build/)

- Open the **meson.build** file in a text editor and verify everything will be compiled in the right place. In particular, 'gimpplugindir' needs to point to your Gimp 2.99 plug-in directory.

- Run the following commands from the Resynthesizer directory:

```
meson _build
cd _build
ninja
ninja install
```

- The Resynthesizer plug-in should now be built and ready to test.

## TODO

- [ ] Port all 9 Python plug-ins (**8** / 9)

- [ ] Clean up code and format it according to the current GIMP style format

- [ ] Submit to main branch of Resynthesizer for approval

## Notes on changes

- **/src/synthesizer/drawable.c**: Update some APIs to 3.0 standard (e.g. gimp_drawable_get_bpp instead of gimp_drawable_bpp)

- **/src/synthesizer/resynthPDBv3.h**: Change plug-in signature to work with multi-selected layer APIs

- **/PluginScripts/**: In-progress conversion of Python plug-ins.

