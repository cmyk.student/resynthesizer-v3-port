#!/usr/bin/env python

'''
Gimp plugin "Heal transparency"

Copyright 2010 lloyd konneker (bootch at nc.rr.com)

Version:
  1.0 lloyd konneker lkk 2010 Initial version in python.

License:

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  The GNU Public License is available at
  http://www.gnu.org/copyleft/gpl.html

'''

import sys

import gi
gi.require_version('Gimp', '3.0')
from gi.repository import Gimp
gi.require_version('GimpUi', '3.0')
from gi.repository import GimpUi
from gi.repository import GObject
from gi.repository import GLib
from gi.repository import Gio
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

def N_(message): return message
def _(message): return GLib.dgettext(None, message)

debug = False

def run(procedure, run_mode, image, n_layers, layers, args, data):
    samplingRadiusParam = args.index(0)
    orderParam = args.index(1)

    progress_bar = None
    config = None

    if run_mode == Gimp.RunMode.INTERACTIVE:

        config = procedure.create_config()

        config.begin_run(image, run_mode, args)

        GimpUi.init("plugin-heal-transparency.py")
        use_header_bar = Gtk.Settings.get_default().get_property("gtk-dialogs-use-header")
        dialog = GimpUi.Dialog(use_header_bar=use_header_bar,
                             title=_("Resynth Heal Transparency"))
        dialog.add_button("_Cancel", Gtk.ResponseType.CANCEL)
        dialog.add_button("_OK", Gtk.ResponseType.OK)

        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL,
                       homogeneous=False, spacing=10)
        dialog.get_content_area().add(vbox)
        vbox.show()

        # Create grid to set all the properties inside.
        grid = Gtk.Grid()
        grid.set_column_homogeneous(False)
        grid.set_border_width(10)
        grid.set_column_spacing(10)
        grid.set_row_spacing(10)
        vbox.add(grid)
        grid.show()

        # Context sampling parameter
        label = Gtk.Label.new_with_mnemonic(_("_Context sampling width (pixels)"))
        grid.attach(label, 0, 0, 1, 1)
        label.show()
        spin = GimpUi.prop_spin_button_new(config, "samplingRadiusParam", step_increment=1, page_increment=1, digits=0)
        grid.attach(spin, 1, 0, 1, 1)
        spin.show()


        # Filling order parameter
        label = Gtk.Label.new_with_mnemonic(_("_Filling order"))
        grid.attach(label, 0, 1, 1, 1)
        label.show()
        combo = GimpUi.IntComboBox.new (["Random", "Inwards towards center", "Outwards from center"])
        if config.get_property("orderParam") != None:
          combo.set_active (config.get_property("orderParam"))
        else:
          combo.set_active (0)
        grid.attach(combo, 1, 1, 1, 1)
        combo.show()

        progress_bar = Gtk.ProgressBar()
        vbox.add(progress_bar)
        progress_bar.show()

        dialog.show()
        if dialog.run() != Gtk.ResponseType.OK:
            return procedure.new_return_values(Gimp.PDBStatusType.CANCEL,
                                               GLib.Error())

        # Extract value from UI
        samplingRadiusParam = config.get_property("samplingRadiusParam")
        orderParam = (combo.get_active ())[1]
        
    #ACTUAL ALGORITHM
    if not layers[0].has_alpha ():
      print(_("The active layer has no alpha channel to heal.")) #Need to find the actual warning function
      return procedure.new_return_values(Gimp.PDBStatusType.CANCEL, GLib.Error())

    Gimp.Image.undo_group_start (image)

    # save selection for later restoration.
    # Saving selection channel makes it active, so we must save and restore the active layer
    org_selection = Gimp.Selection.save (image)
    image.set_selected_layers(layers)

    # alpha to selection
    Gimp.Image.select_item(image, Gimp.ChannelOps.REPLACE, layers[0])
    # Want the transparent, not the opaque.
    Gimp.Selection.invert (image)
    # Since transparency was probably anti-aliased (dithered with partial transpancy),
    # grow the selection to get past the dithering.
    Gimp.Selection.grow(image, 1)
    # Remove the alpha from this layer. IE compose with current background color (often white.)
    # Resynthesizer won't heal transparent.
    Gimp.Layer.flatten(layers[0])

    # Call heal selection (not the resynthesizer), which will create a proper corpus.
    # 0 = sample from all around
    # gimpfu provides run_mode NONINTERACTIVE
    allAround = 0
    
    result = Gimp.get_pdb().run_procedure('resynth-heal-selection', [
        GObject.Value(Gimp.RunMode, Gimp.RunMode.NONINTERACTIVE),
        GObject.Value(Gimp.Image, image),
        GObject.Value(GObject.TYPE_INT, n_layers),
        GObject.Value(Gimp.ObjectArray, Gimp.ObjectArray.new(Gimp.Drawable, [layers[0]], False)),
        GObject.Value(GObject.TYPE_INT, samplingRadiusParam),
        GObject.Value(GObject.TYPE_INT, allAround),
        GObject.Value(GObject.TYPE_INT, orderParam),
    ])

    # Restore image to initial conditions of user, except for later cleanup.
    # restore selection
    Gimp.Image.select_item(image, Gimp.ChannelOps.REPLACE, layers[0])
    
    Gimp.Image.undo_group_end(image)
    #END ACTUAL ALGORITHM

    # If the execution was successful, save parameters so they will be restored next time we show the dialog.
    #if result.index(0) == Gimp.PDBStatusType.SUCCESS and config is not None:
    config.set_property("orderParam", orderParam)
    config.end_run(Gimp.PDBStatusType.SUCCESS)

    return procedure.new_return_values(Gimp.PDBStatusType.SUCCESS, GLib.Error())

class ResynthHealTransparency(Gimp.PlugIn):

    ## Parameters ##
    __gproperties__ = {
        "samplingRadiusParam":  (int,
                                 _("_Context sampling width (pixels)"),
                                 "Context sampling width (pixels)",
                                 1, 2342, 50,
                                 GObject.ParamFlags.READWRITE),
        "orderParam":  (int,
                                 _("_Filling order { Random (0), Inwards towards center (1), Outwards from center (2) }"),
                                 "Filling order { Random (0), Inwards towards center (1), Outwards from center (2) }",
                                 0, 2, 0,
                                 GObject.ParamFlags.READWRITE),
    }

    ## GimpPlugIn virtual methods ##
    def do_set_i18n(self, procname):
        return True, 'gimp30-python', None

    def do_query_procedures(self):
        return ['resynth-heal-transparency']

    def do_create_procedure(self, name):
        procedure = None
        if name == 'resynth-heal-transparency':
            procedure = Gimp.ImageProcedure.new(self, name,
                                                Gimp.PDBProcType.PLUGIN,
                                                run, None)

            procedure.set_image_types("RGBA, GRAYA")
            procedure.set_documentation (
                N_("Removes alpha channel by synthesis.  Fill outward for edges, inward for holes."),
                globals()["__doc__"],  # This includes the docstring, on the top of the file
                name)
            procedure.set_menu_label(N_("_Heal transparency..."))
            procedure.set_attribution("Lloyd Konneker",
                                      "(c) GPL V3.0 or later",
                                      "2010")
            procedure.add_menu_path("<Image>/Filters/Enhance/")

            procedure.add_argument_from_property(self, "samplingRadiusParam")
            procedure.add_argument_from_property(self, "orderParam")

        return procedure

Gimp.main(ResynthHealTransparency.__gtype__, sys.argv)

