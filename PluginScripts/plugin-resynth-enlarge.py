#!/usr/bin/env python

'''
Gimp plugin "Enlarge and resynthesize"

Author:
lloyd konneker
Based on smart_enlarge.scm 2000 by Paul Harrison.

Version:
1.0 lloyd konneker lkk 2010 Initial version in python.

License:

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

The GNU Public License is available at
http://www.gnu.org/copyleft/gpl.html
'''

import sys

import gi
gi.require_version('Gimp', '3.0')
from gi.repository import Gimp
gi.require_version('GimpUi', '3.0')
from gi.repository import GimpUi
from gi.repository import GObject
from gi.repository import GLib
from gi.repository import Gio
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

def N_(message): return message
def _(message): return GLib.dgettext(None, message)

def run(procedure, run_mode, image, n_layers, layers, args, data):
    scale_factor = args.index(0)

    progress_bar = None
    config = None

    if run_mode == Gimp.RunMode.INTERACTIVE:

        config = procedure.create_config()

        config.begin_run(image, run_mode, args)

        GimpUi.init("plugin-resynth-enlarge.py")
        use_header_bar = Gtk.Settings.get_default().get_property("gtk-dialogs-use-header")
        dialog = GimpUi.Dialog(use_header_bar=use_header_bar,
                             title=_("Resynth Enlarge"))
        dialog.add_button("_Cancel", Gtk.ResponseType.CANCEL)
        dialog.add_button("_OK", Gtk.ResponseType.OK)

        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL,
                       homogeneous=False, spacing=10)
        dialog.get_content_area().add(vbox)
        vbox.show()

        # Create grid to set all the properties inside.
        grid = Gtk.Grid()
        grid.set_column_homogeneous(False)
        grid.set_border_width(10)
        grid.set_column_spacing(10)
        grid.set_row_spacing(10)
        vbox.add(grid)
        grid.show()

        # Sharpen Factor parameter
        label = Gtk.Label.new_with_mnemonic(_("_Scale Factor"))
        grid.attach(label, 0, 0, 1, 1)
        label.show()
        spin = GimpUi.prop_spin_button_new(config, "scale_factor", step_increment=0.01, page_increment=0.1, digits=2)
        grid.attach(spin, 1, 0, 1, 1)
        spin.show()

        progress_bar = Gtk.ProgressBar()
        vbox.add(progress_bar)
        progress_bar.show()

        dialog.show()
        if dialog.run() != Gtk.ResponseType.OK:
            return procedure.new_return_values(Gimp.PDBStatusType.CANCEL,
                                               GLib.Error())

        # Extract value from UI
        scale_factor = config.get_property("scale_factor")
        
    #ACTUAL ALGORITHM
    '''
    Algorithm:

    Scale image up.
    Resynthesize with:
        corpus = original size image
        in map = original size image but scaled up and down to blur
        out map = scaled up image

    This restores the detail that scaling up looses.
    It maintains the aspect ratio of all image features.

    Unlike the original smart-enlarge.scm, this alters the original image.

    original did not accept an alpha channel
    '''
    temp_image1 = Gimp.Image.duplicate(image)  # duplicate for in map
    if not temp_image1:
       raise RuntimeError("Failed duplicate image")
    temp_image2 = Gimp.Image.duplicate(image)  # duplicate for corpus
    if not temp_image2:
       raise RuntimeError("Failed duplicate image")
    temp_layer1 = temp_image1.list_selected_layers()
    temp_layer2 = temp_image2.list_selected_layers()

    width = layers[0].get_width()
    height = layers[0].get_height()

    # scale input map down and back, to blur
    Gimp.Image.scale(temp_image1, width/scale_factor, height/scale_factor)
    Gimp.Image.scale(temp_image1, width, height)

    # scale up the image
    Gimp.Image.scale(image, width * scale_factor, height * scale_factor)
    
    #Placeholder values
    negative = False
    border = 0
    t1 = 1.0
    t2 = 0.117
    t3 = 8
    t4 = 500

    # Resynthesize to restore details.
    result = Gimp.get_pdb().run_procedure('plug-in-resynthesizer', [
        GObject.Value(Gimp.RunMode, Gimp.RunMode.NONINTERACTIVE),
        GObject.Value(Gimp.Image, image),
        GObject.Value(GObject.TYPE_INT, n_layers),
        GObject.Value(Gimp.ObjectArray, Gimp.ObjectArray.new(Gimp.Drawable, [layers[0]], False)),
        GObject.Value(GObject.TYPE_BOOLEAN, negative),
        GObject.Value(GObject.TYPE_BOOLEAN, negative),
        GObject.Value(GObject.TYPE_INT, border),
        GObject.Value(Gimp.Drawable, temp_layer2[0]),
        GObject.Value(Gimp.Drawable, temp_layer1[0]),
        GObject.Value(Gimp.Drawable, layers[0]),
        GObject.Value(GObject.TYPE_DOUBLE, t1),
        GObject.Value(GObject.TYPE_DOUBLE, t2),
        GObject.Value(GObject.TYPE_INT, t3),
        GObject.Value(GObject.TYPE_INT, t4),
    ])    

    Gimp.Image.delete(temp_image1)
    Gimp.Image.delete(temp_image2)
    #END ACTUAL ALGORITHM

    # If the execution was successful, save parameters so they will be restored next time we show the dialog.
    #if result.index(0) == Gimp.PDBStatusType.SUCCESS and config is not None:
    config.end_run(Gimp.PDBStatusType.SUCCESS)

    return procedure.new_return_values(Gimp.PDBStatusType.SUCCESS, GLib.Error())

class ResynthEnlarge(Gimp.PlugIn):

    ## Parameters ##
    __gproperties__ = {
        "scale_factor":  (float,
                          _("_Scale by"),
                          "Scale by",
                          1.0, 32.0, 2.0,
                          GObject.ParamFlags.READWRITE),
    }

    ## GimpPlugIn virtual methods ##
    def do_set_i18n(self, procname):
        return True, 'gimp30-python', None

    def do_query_procedures(self):
        return ['resynth-enlarge']

    def do_create_procedure(self, name):
        procedure = None
        if name == 'resynth-enlarge':
            procedure = Gimp.ImageProcedure.new(self, name,
                                                Gimp.PDBProcType.PLUGIN,
                                                run, None)

            procedure.set_image_types("RGB*, GRAY*")
            procedure.set_documentation (
                N_("Enlarge image and synthesize to sharpen."),
                globals()["__doc__"],  # This includes the docstring, on the top of the file
                name)
            procedure.set_menu_label(N_("_Enlarge & sharpen..."))
            procedure.set_attribution("Lloyd Konneker",
                                      "(c) GPL V3.0 or later",
                                      "2010")
            procedure.add_menu_path("<Image>/Filters/Enhance/")

            procedure.add_argument_from_property(self, "scale_factor")

        return procedure

Gimp.main(ResynthEnlarge.__gtype__, sys.argv)
