#!/usr/bin/env python

'''
Gimp plugin "Heal selection"

Copyright 2009 lloyd konneker (bootch at nc.rr.com)
Based on smart_remove.scm Copyright 2000 by Paul Harrison.

Version:
  1.0 lloyd konneker lkk 9/21/2009 Initial version in python.
  (See release notes for differences over P. Harrison's prior version in scheme language.)

License:

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  The GNU Public License is available at
  http://www.gnu.org/copyleft/gpl.html

'''

import sys

import gi
gi.require_version('Gimp', '3.0')
from gi.repository import Gimp
gi.require_version('GimpUi', '3.0')
from gi.repository import GimpUi
from gi.repository import GObject
from gi.repository import GLib
from gi.repository import Gio
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

def N_(message): return message
def _(message): return GLib.dgettext(None, message)

debug = False

def run(procedure, run_mode, image, n_layers, layers, args, data):
    samplingRadiusParam = args.index(0)
    directionParam = args.index(1)
    orderParam = args.index(2)

    progress_bar = None
    config = None

    if run_mode == Gimp.RunMode.INTERACTIVE:

        config = procedure.create_config()

        config.begin_run(image, run_mode, args)

        GimpUi.init("plugin-heal-selection.py")
        use_header_bar = Gtk.Settings.get_default().get_property("gtk-dialogs-use-header")
        dialog = GimpUi.Dialog(use_header_bar=use_header_bar,
                             title=_("Resynth Heal Selection"))
        dialog.add_button("_Cancel", Gtk.ResponseType.CANCEL)
        dialog.add_button("_OK", Gtk.ResponseType.OK)

        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL,
                       homogeneous=False, spacing=10)
        dialog.get_content_area().add(vbox)
        vbox.show()

        # Create grid to set all the properties inside.
        grid = Gtk.Grid()
        grid.set_column_homogeneous(False)
        grid.set_border_width(10)
        grid.set_column_spacing(10)
        grid.set_row_spacing(10)
        vbox.add(grid)
        grid.show()

        # Context sampling parameter
        label = Gtk.Label.new_with_mnemonic(_("_Context sampling width (pixels)"))
        grid.attach(label, 0, 0, 1, 1)
        label.show()
        spin = GimpUi.prop_spin_button_new(config, "samplingRadiusParam", step_increment=1, page_increment=1, digits=0)
        grid.attach(spin, 1, 0, 1, 1)
        spin.show()

        # Sample from parameter
        label = Gtk.Label.new_with_mnemonic(_("_Sample from"))
        grid.attach(label, 0, 1, 1, 1)
        label.show()
        combo1 = GimpUi.IntComboBox.new (["All around", "Sides", "Above and below"])
        if config.get_property("directionParam") != None:
          combo1.set_active (config.get_property("directionParam"))
        else:
          combo1.set_active (0)
        grid.attach(combo1, 1, 1, 1, 1)
        combo1.show()

        # Filling order parameter
        label = Gtk.Label.new_with_mnemonic(_("_Filling order"))
        grid.attach(label, 0, 2, 1, 1)
        label.show()
        combo2 = GimpUi.IntComboBox.new (["Random", "Inwards towards center", "Outwards from center"])
        if config.get_property("orderParam") != None:
          combo2.set_active (config.get_property("orderParam"))
        else:
          combo2.set_active (0)
        grid.attach(combo2, 1, 2, 1, 1)
        combo2.show()

        progress_bar = Gtk.ProgressBar()
        vbox.add(progress_bar)
        progress_bar.show()

        dialog.show()
        if dialog.run() != Gtk.ResponseType.OK:
            return procedure.new_return_values(Gimp.PDBStatusType.CANCEL,
                                               GLib.Error())

        # Extract value from UI
        samplingRadiusParam = config.get_property("samplingRadiusParam")
        directionParam = (combo1.get_active ())[1]
        orderParam = (combo2.get_active ())[1]
        
    #ACTUAL ALGORITHM
    if Gimp.Selection.is_empty (image):
      print(_("You must first select a region to heal.")) #Need to find the actual warning function
      return procedure.new_return_values(Gimp.PDBStatusType.CANCEL, GLib.Error())

    Gimp.Image.undo_group_start (image)

    targetBounds = layers[0].mask_bounds ()

    # In duplicate image, create the sample (corpus).
    # (I tried to use a temporary layer but found it easier to use duplicate image.)
    tempImage = Gimp.Image.duplicate(image)  # duplicate for in map
    if not tempImage:
       raise RuntimeError("Failed duplicate image")

    # !!! The drawable can be a mask (grayscale channel), don't restrict to layer.
    work_drawables = Gimp.Image.get_selected_drawables(tempImage)
    if not work_drawables:
      raise RuntimeError("Failed get active drawables")
    '''
    grow and punch hole, making a frisket iow stencil iow donut

    '''
    orgSelection = Gimp.Selection.save(tempImage) # save for later use
    Gimp.Selection.grow(tempImage, samplingRadiusParam)
    # ??? returns None , docs say it returns SUCCESS

    # !!! Note that if selection is a bordering ring already, growing expanded it inwards.
    # Which is what we want, to make a corpus inwards.

    grownSelection = Gimp.Selection.save(tempImage)

    # Cut hole where the original selection was, so we don't sample from it.
    # !!! Note that gimp enums/constants are not prefixed with GIMP_
    Gimp.Image.select_item(tempImage, Gimp.ChannelOps.SUBTRACT, orgSelection)

    '''
    Selection (to be the corpus) is donut or frisket around the original target T
        xxx
        xTx
        xxx
    '''

    Gimp.Selection.invert (tempImage)

    # crop the temp image to size of selection to save memory and for directional healing!!
    frisketBounds = grownSelection.mask_bounds ()
    frisketLowerLeftX = frisketBounds[0]
    frisketLowerLeftY = frisketBounds[1]
    frisketUpperRightX = frisketBounds[2]
    frisketUpperRightY = frisketBounds[3]
    targetLowerLeftX = targetBounds[0]
    targetLowerLeftY = targetBounds[1]
    targetUpperRightX = targetBounds[2]
    targetUpperRightY = targetBounds[3]

    frisketWidth = frisketUpperRightX - frisketLowerLeftX
    frisketHeight = frisketUpperRightY - frisketLowerLeftY

    # User's choice of direction affects the corpus shape, and is also passed to resynthesizer plugin
    if directionParam == 0: # all around
      # Crop to the entire frisket
      newWidth, newHeight, newLLX, newLLY = ( frisketWidth, frisketHeight,
        frisketLowerLeftX, frisketLowerLeftY )
    elif directionParam == 1: # sides
      # Crop to target height and frisket width:  XTX
      newWidth, newHeight, newLLX, newLLY =  ( frisketWidth, targetUpperRightY-targetLowerLeftY,
        frisketLowerLeftX, targetLowerLeftY )
    elif directionParam == 2: # above and below
      # X Crop to target width and frisket height
      # T
      # X
      newWidth, newHeight, newLLX, newLLY = ( targetUpperRightX-targetLowerLeftX, frisketHeight,
        targetLowerLeftX, frisketLowerLeftY )
    # Restrict crop to image size (condition of gimp_image_crop) eg when off edge of image
    newWidth = min(tempImage.get_width () - newLLX, newWidth)
    newHeight = min(tempImage.get_height () - newLLY, newHeight)
    #Gimp.Image.crop(tempImage, newWidth, newHeight, newLLX, newLLY)

    # Encode two script params into one resynthesizer param.
    # use border 1 means fill target in random order
    # use border 0 is for texture mapping operations, not used by this script
    if not orderParam :
      useBorder = 1   # User wants NO order, ie random filling
    elif orderParam == 1 :  # Inward to corpus.  2,3,4
      useBorder = directionParam+2   # !!! Offset by 2 to get past the original two boolean values
    else:
      # Outward from image center.
      # 5+0=5 outward concentric
      # 5+1=6 outward from sides
      # 5+2=7 outward above and below
      useBorder = directionParam+5

    #Placeholder values
    negative = False
    t1 = 0.0
    t2 = 0.117
    t3 = 16
    t4 = 500

    # Not necessary to restore image to initial condition of selection, activity,
    # the original image should not have been changed,
    # and the resynthesizer should only heal, not change selection.

    # Note that the API hasn't changed but use_border param now has more values.
    result = Gimp.get_pdb().run_procedure('plug-in-resynthesizer', [
        GObject.Value(Gimp.RunMode, Gimp.RunMode.NONINTERACTIVE),
        GObject.Value(Gimp.Image, image),
        GObject.Value(GObject.TYPE_INT, n_layers),
        GObject.Value(Gimp.ObjectArray, Gimp.ObjectArray.new(Gimp.Drawable, [layers[0]], False)),
        GObject.Value(GObject.TYPE_BOOLEAN, negative),
        GObject.Value(GObject.TYPE_BOOLEAN, negative),
        GObject.Value(GObject.TYPE_INT, useBorder),
        GObject.Value(Gimp.Drawable, work_drawables[0]),
        GObject.Value(Gimp.Drawable, None),
        GObject.Value(Gimp.Drawable, None),
        GObject.Value(GObject.TYPE_DOUBLE, t1),
        GObject.Value(GObject.TYPE_DOUBLE, t2),
        GObject.Value(GObject.TYPE_INT, t3),
        GObject.Value(GObject.TYPE_INT, t4),
    ]) 

    # Clean up (comment out to debug)
    Gimp.Image.delete(tempImage)
    Gimp.Image.undo_group_end(image)

    #END ACTUAL ALGORITHM

    # If the execution was successful, save parameters so they will be restored next time we show the dialog.
    #if result.index(0) == Gimp.PDBStatusType.SUCCESS and config is not None:
    config.set_property("directionParam", directionParam)
    config.set_property("orderParam", orderParam)
    config.end_run(Gimp.PDBStatusType.SUCCESS)

    return procedure.new_return_values(Gimp.PDBStatusType.SUCCESS, GLib.Error())

class ResynthHealSelection(Gimp.PlugIn):

    ## Parameters ##
    __gproperties__ = {
        "samplingRadiusParam":  (int,
                                 _("_Context sampling width (pixels)"),
                                 "Context sampling width (pixels)",
                                 1, 2342, 50,
                                 GObject.ParamFlags.READWRITE),
        "directionParam":  (int,
                                 _("_Sample from { All around (0), Sides (1), Above and below (2) }"),
                                 "Sample from { All around (0), Sides (1), Above and below (2) }",
                                 0, 2, 0,
                                 GObject.ParamFlags.READWRITE),
        "orderParam":  (int,
                                 _("_Filling order { Random (0), Inwards towards center (1), Outwards from center (2) }"),
                                 "Filling order { Random (0), Inwards towards center (1), Outwards from center (2) }",
                                 0, 2, 0,
                                 GObject.ParamFlags.READWRITE),
    }

    ## GimpPlugIn virtual methods ##
    def do_set_i18n(self, procname):
        return True, 'gimp30-python', None

    def do_query_procedures(self):
        return ['resynth-heal-selection']

    def do_create_procedure(self, name):
        procedure = None
        if name == 'resynth-heal-selection':
            procedure = Gimp.ImageProcedure.new(self, name,
                                                Gimp.PDBProcType.PLUGIN,
                                                run, None)

            procedure.set_image_types("RGB*, GRAY*")
            procedure.set_documentation (
                N_("Heal the selection from surroundings as if using the heal tool."),
                globals()["__doc__"],  # This includes the docstring, on the top of the file
                name)
            procedure.set_menu_label(N_("_Heal selection..."))
            procedure.set_attribution("Lloyd Konneker",
                                      "(c) GPL V3.0 or later",
                                      "2009")
            procedure.add_menu_path("<Image>/Filters/Enhance/")

            procedure.add_argument_from_property(self, "samplingRadiusParam")
            procedure.add_argument_from_property(self, "directionParam")
            procedure.add_argument_from_property(self, "orderParam")

        return procedure

Gimp.main(ResynthHealSelection.__gtype__, sys.argv)

