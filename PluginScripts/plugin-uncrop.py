#!/usr/bin/env python

'''
Gimp plugin "Uncrop"

Increase image/canvas size and synthesize outer band from edge of original.

Author:
lloyd konneker, lkk

Version:
1.0 lkk 5/15/2009 Initial version in scheme, released to Gimp Registry.
1.1 lkk 9/21/2009 Translate to python.

License:

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

The GNU Public License is available at
http://www.gnu.org/copyleft/gpl.html


The effect for users:
widens the field of view, maintaining perspective of original
Should be undoable, except for loss of selection.
Should work on any image type, any count of layers and channels (although only active layer is affected.)

Programming notes:
Scheme uses - in names, python uses _
Programming devt. cycle:
Initial creation: cp foo.py ~/.gimp-2.6/scripts, chmod +x, start gimp
Refresh:  just copy, no need to restart gimp if the pdb registration is unchanged

IN: Nothing special.  The selection is immaterial but is not preserved.
OUT larger layer and image.  All other layers not enlarged.
'''

import sys

import gi
gi.require_version('Gimp', '3.0')
from gi.repository import Gimp
gi.require_version('GimpUi', '3.0')
from gi.repository import GimpUi
from gi.repository import GObject
from gi.repository import GLib
from gi.repository import Gio
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

def N_(message): return message
def _(message): return GLib.dgettext(None, message)

def resizeImageCentered(image, percentEnlarge):
    # resize and center image by percent (converted to pixel units)
    deltaFraction = (percentEnlarge / 100) + 1.0
    priorWidth = image.get_width()
    priorHeight = image.get_height()
    deltaWidth = priorWidth * deltaFraction
    deltaHeight = priorHeight * deltaFraction
    centeredOffX = (deltaWidth - priorWidth)/  2
    centeredOffY = (deltaHeight - priorHeight) / 2
    image.resize(deltaWidth, deltaHeight, centeredOffX, centeredOffY)

def shrinkSelectionByPercent(image, percent):
    # shrink selection by percent (converted to pixel units)
    deltaFraction = percent / 100
    # convert to pixel dimensions
    priorWidth = image.get_width()
    priorHeight = image.get_height()
    deltaWidth = priorWidth * deltaFraction
    deltaHeight = priorHeight * deltaFraction
    # !!! Note total shrink percentage is halved (width of band is percentage/2)
    maxDelta = max(deltaWidth, deltaHeight) / 2

    Gimp.Selection.shrink(image, maxDelta)

def run(procedure, run_mode, image, n_layers, layers, args, data):
    percentEnlargeParam = args.index(0)

    progress_bar = None
    config = None

    if run_mode == Gimp.RunMode.INTERACTIVE:

        config = procedure.create_config()

        config.begin_run(image, run_mode, args)

        GimpUi.init("plugin-uncrop.py")
        use_header_bar = Gtk.Settings.get_default().get_property("gtk-dialogs-use-header")
        dialog = GimpUi.Dialog(use_header_bar=use_header_bar,
                             title=_("Uncrop"))
        dialog.add_button("_Cancel", Gtk.ResponseType.CANCEL)
        dialog.add_button("_OK", Gtk.ResponseType.OK)

        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL,
                       homogeneous=False, spacing=10)
        dialog.get_content_area().add(vbox)
        vbox.show()

        # Create grid to set all the properties inside.
        grid = Gtk.Grid()
        grid.set_column_homogeneous(False)
        grid.set_border_width(10)
        grid.set_column_spacing(10)
        grid.set_row_spacing(10)
        vbox.add(grid)
        grid.show()

        # Sharpen Factor parameter
        label = Gtk.Label.new_with_mnemonic(_("_Percent enlargement"))
        grid.attach(label, 0, 0, 1, 1)
        label.show()
        spin = GimpUi.prop_spin_button_new(config, "percentEnlargeParam", step_increment=0.01, page_increment=0.1, digits=2)
        grid.attach(spin, 1, 0, 1, 1)
        spin.show()

        progress_bar = Gtk.ProgressBar()
        vbox.add(progress_bar)
        progress_bar.show()

        dialog.show()
        if dialog.run() != Gtk.ResponseType.OK:
            return procedure.new_return_values(Gimp.PDBStatusType.CANCEL,
                                               GLib.Error())

        # Extract value from UI
        percentEnlargeParam = config.get_property("percentEnlargeParam")

    #ACTUAL ALGORITHM
    '''
    Create frisket stencil selection in a temp image to pass as source (corpus) to plugin resynthesizer,
    which does the substantive work.
    '''

    if not layers[0].is_layer():
      Gimp.message ("A layer must be active, not a channel.")
      return

    Gimp.Image.undo_group_start (image)

    # copy original into temp for later use
    tempImage = Gimp.Image.duplicate(image)
    if not tempImage:
       raise RuntimeError("Failed duplicate image")

    '''
    Prepare target: enlarge canvas and select the new, blank outer ring
    '''

    # Save original bounds to later select outer band
    Gimp.Selection.all(image)
    selectAllPrior = Gimp.Selection.save(image)
    # Resize image alone doesn't resize layer, so resize layer also
    resizeImageCentered(image, percentEnlargeParam)
    Gimp.Layer.resize_to_image_size(layers[0])
    image.select_item(Gimp.ChannelOps.REPLACE, selectAllPrior)
    # select outer band, the new blank canvas.
    Gimp.Selection.invert(image)
    # Assert target image is ready.

    '''
    Prepare source (corpus) layer, a band at edge of original, in a dupe.
    Note the width of corpus band is same as width of enlargement band.
    '''
    # Working with the original size.
    # Could be alpha channel transparency
    workLayer = tempImage.list_selected_layers()
    if not workLayer:
        raise RuntimeError("Failed get active layer")
    # Select outer band:  select all, shrink
    Gimp.Selection.all(tempImage)
    shrinkSelectionByPercent(tempImage, percentEnlargeParam)
    Gimp.Selection.invert(tempImage)    # invert interior selection into a frisket
    # Note that v1 resynthesizer required an inverted selection !!
    # No need to crop corpus to save memory.

    # Note that the API hasn't changed but use_border param now has more values.
    # !!! The crux: use_border param=5 means inside out direction
    negative = False
    border = 5
    nullLayers = None
    t1 = 0
    t2 = 0.117
    t3 = 16
    t4 = 500

    result = Gimp.get_pdb().run_procedure('plug-in-resynthesizer', [
        GObject.Value(Gimp.RunMode, Gimp.RunMode.NONINTERACTIVE),
        GObject.Value(Gimp.Image, image),
        GObject.Value(GObject.TYPE_INT, n_layers),
        GObject.Value(Gimp.ObjectArray, Gimp.ObjectArray.new(Gimp.Drawable, [layers[0]], False)),
        GObject.Value(GObject.TYPE_BOOLEAN, negative),
        GObject.Value(GObject.TYPE_BOOLEAN, negative),
        GObject.Value(GObject.TYPE_INT, border),
        GObject.Value(Gimp.Drawable, workLayer[0]),
        GObject.Value(Gimp.Drawable, nullLayers),
        GObject.Value(Gimp.Drawable, nullLayers),
        GObject.Value(GObject.TYPE_DOUBLE, t1),
        GObject.Value(GObject.TYPE_DOUBLE, t2),
        GObject.Value(GObject.TYPE_INT, t3),
        GObject.Value(GObject.TYPE_INT, t4),
    ])

    # Clean up.
    # Any errors now are moot.
    Gimp.Selection.none(image)
    Gimp.Image.remove_channel(image, selectAllPrior)
    Gimp.Image.undo_group_end(image)
    Gimp.Image.delete(tempImage)
    #END ACTUAL ALGORITHM

    # If the execution was successful, save parameters so they will be restored next time we show the dialog.
    #if result.index(0) == Gimp.PDBStatusType.SUCCESS and config is not None:
    config.end_run(Gimp.PDBStatusType.SUCCESS)

    return procedure.new_return_values(Gimp.PDBStatusType.SUCCESS, GLib.Error())

class Uncrop(Gimp.PlugIn):

    ## Parameters ##
    __gproperties__ = {
        "percentEnlargeParam":  (float,
                                 _("_Percent enlargement"),
                                 "Percent enlargement",
                                 0.0, 100.0, 10.0,
                                 GObject.ParamFlags.READWRITE),
    }

    ## GimpPlugIn virtual methods ##
    def do_set_i18n(self, procname):
        return True, 'gimp30-python', None

    def do_query_procedures(self):
        return ['uncrop']

    def do_create_procedure(self, name):
        procedure = None
        if name == 'uncrop':
            procedure = Gimp.ImageProcedure.new(self, name,
                                                Gimp.PDBProcType.PLUGIN,
                                                run, None)

            procedure.set_image_types("RGB*, GRAY*")
            procedure.set_documentation (
                N_("Enlarge image by synthesizing a border that matches the edge, maintaining perspective.  Works best for small enlargement of natural edges. Undo a Crop instead, if possible!"),
                globals()["__doc__"],  # This includes the docstring, on the top of the file
                name)
            procedure.set_menu_label(N_("_Uncrop..."))
            procedure.set_attribution("Lloyd Konneker",
                                      "(c) GPL V3.0 or later",
                                      "2009")
            procedure.add_menu_path("<Image>/Filters/Enhance/")

            procedure.add_argument_from_property(self, "percentEnlargeParam")

        return procedure

Gimp.main(Uncrop.__gtype__, sys.argv)
