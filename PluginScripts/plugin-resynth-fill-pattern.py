#!/usr/bin/env python

'''
Gimp plugin "Fill with pattern seamless..."
Front end to the resynthesizer plugin to make a seamless fill.

Copyright 2011 lloyd konneker
Idea by Rob Antonishen

Version:
1.0 lloyd konneker

During development, remember to make it executable!!!
And to remove older versions, both .scm and .py that might hide it.

License:

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

The GNU Public License is available at
http://www.gnu.org/copyleft/gpl.html

'''

import sys

import gi
gi.require_version('Gimp', '3.0')
from gi.repository import Gimp
gi.require_version('GimpUi', '3.0')
from gi.repository import GimpUi
from gi.repository import GObject
from gi.repository import GLib
from gi.repository import Gio
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

def N_(message): return message
def _(message): return GLib.dgettext(None, message)

debug = False

def layer_from_pattern(image, pattern):
  '''
  Create a new image and layer having the same size as a pattern.
  '''
  new_basetype = Gimp.Image.get_base_type(image)  # same as source
  selected_layers = Gimp.Image.list_selected_layers(image)

  new_layertype = Gimp.Drawable.type(selected_layers[0])
  result = Gimp.get_pdb().run_procedure('gimp-pattern-get-info',
     [ GObject.Value(GObject.TYPE_STRING,
       pattern),
     ])
  pattern_width = result.index (1)
  pattern_height = result.index (2)
  bpp = result.index (3)

  new_image = Gimp.Image.new(pattern_width, pattern_height, new_basetype)
  # !!! Note that gimp_layer_new wants a layer type, not an image basetype

  new_drawable = Gimp.Layer.new(new_image, "Texture", pattern_width, pattern_height,
    new_layertype, 100, Gimp.LayerMode.NORMAL)
  new_image.insert_layer(new_drawable, None, 0)
  return new_image, new_drawable

def guts(image, n_layers, layers, pattern):
  ''' Crux of algorithm '''

  # Make drawble from pattern
  pattern_image, pattern_layer = layer_from_pattern(image, pattern)

  # Fill it with pattern
  # NOT pass pattern_layer.ID !!!
  Gimp.Drawable.fill(pattern_layer, Gimp.FillType.PATTERN)

  # Resynthesize the selection from the pattern without using context
  # 0,0,0: Not use_border (context), not tile horiz, not tile vert
  # -1, -1, 0: No maps and no map weight
  # DO pass pattern_layer.ID !!!
  # Resynthesizer is an engine, never interactive

  #Placeholder values
  negative = False
  border = 0
  t1 = 0.0
  t2 = 0.05
  t3 = 8
  t4 = 300

  result = Gimp.get_pdb().run_procedure('plug-in-resynthesizer', [
    GObject.Value(Gimp.RunMode, Gimp.RunMode.NONINTERACTIVE),
    GObject.Value(Gimp.Image, image),
    GObject.Value(GObject.TYPE_INT, n_layers),
    GObject.Value(Gimp.ObjectArray, Gimp.ObjectArray.new(Gimp.Drawable, [layers[0]], False)),
    GObject.Value(GObject.TYPE_BOOLEAN, negative),
    GObject.Value(GObject.TYPE_BOOLEAN, negative),
    GObject.Value(GObject.TYPE_INT, border),
    GObject.Value(Gimp.Drawable, pattern_layer),
    GObject.Value(Gimp.Drawable, None),
    GObject.Value(Gimp.Drawable, None),
    GObject.Value(GObject.TYPE_DOUBLE, t1),
    GObject.Value(GObject.TYPE_DOUBLE, t2),
    GObject.Value(GObject.TYPE_INT, t3),
    GObject.Value(GObject.TYPE_INT, t4),
  ])

  # Clean up
  if not debug:
    # Delete image that is displayed throws RuntimeError
    Gimp.Image.delete(pattern_image)


def run(procedure, run_mode, image, n_layers, layers, args, data):
    pattern = args.index(0)

    progress_bar = None
    config = None

    if run_mode == Gimp.RunMode.INTERACTIVE:

        config = procedure.create_config()

        config.begin_run(image, run_mode, args)

        GimpUi.init("plugin-resynth-fill-pattern.py")
        use_header_bar = Gtk.Settings.get_default().get_property("gtk-dialogs-use-header")
        dialog = GimpUi.Dialog(use_header_bar=use_header_bar,
                             title=_("Resynth Fill Pattern"))
        dialog.add_button("_Cancel", Gtk.ResponseType.CANCEL)
        dialog.add_button("_OK", Gtk.ResponseType.OK)

        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL,
                       homogeneous=False, spacing=10)
        dialog.get_content_area().add(vbox)
        vbox.show()

        # Create grid to set all the properties inside.
        grid = Gtk.Grid()
        grid.set_column_homogeneous(False)
        grid.set_border_width(10)
        grid.set_column_spacing(10)
        grid.set_row_spacing(10)
        vbox.add(grid)
        grid.show()

        # Pattern parameter
        label = Gtk.Label.new_with_mnemonic(_("_Pattern"))
        grid.attach(label, 0, 0, 1, 1)
        label.show()
        prior_pattern = "Maple Leaves"
        if config.get_property ("pattern") != None:
          prior_pattern = config.get_property ("pattern")
        button = GimpUi.PatternSelectButton.new ("pattern", prior_pattern)
        grid.attach(button, 1, 0, 1, 1)
        button.show()

        progress_bar = Gtk.ProgressBar()
        vbox.add(progress_bar)
        progress_bar.show()

        dialog.show()
        if dialog.run() != Gtk.ResponseType.OK:
            return procedure.new_return_values(Gimp.PDBStatusType.CANCEL,
                                               GLib.Error())

        # Extract value from UI
        pattern = button.get_pattern ()
        config.set_property ("pattern", pattern)

    #ACTUAL ALGORITHM
    # User_friendly: if no selection, use entire image.
    # But the resynthesizer does that for us.

    # Save/restore the context since we change the pattern
    Gimp.get_pdb().run_procedure('gimp-context-push', [])
    Gimp.get_pdb().run_procedure('gimp-context-set-pattern',
       [ GObject.Value(GObject.TYPE_STRING,
         pattern),
       ])
    guts(image, n_layers, layers, pattern)
    Gimp.get_pdb().run_procedure('gimp-context-pop', [])
    #END ACTUAL ALGORITHM

    # If the execution was successful, save parameters so they will be restored next time we show the dialog.
    #if result.index(0) == Gimp.PDBStatusType.SUCCESS and config is not None:
    config.end_run(Gimp.PDBStatusType.SUCCESS)

    return procedure.new_return_values(Gimp.PDBStatusType.SUCCESS, GLib.Error())

class ResynthFillPattern(Gimp.PlugIn):

    ## Parameters ##
    __gproperties__ = {
        "pattern": (str,
                     _("Pattern"), "", "",
                     GObject.ParamFlags.READWRITE),
    }

    ## GimpPlugIn virtual methods ##
    def do_set_i18n(self, procname):
        return True, 'gimp30-python', None

    def do_query_procedures(self):
        return ['resynth-fill-pattern']

    def do_create_procedure(self, name):
        procedure = None
        if name == 'resynth-fill-pattern':
            procedure = Gimp.ImageProcedure.new(self, name,
                                                Gimp.PDBProcType.PLUGIN,
                                                run, None)

            procedure.set_image_types("RGB*, GRAY*")
            procedure.set_documentation (
                N_("Seamlessly fill with a pattern using synthesis."),
                globals()["__doc__"],  # This includes the docstring, on the top of the file
                name)
            procedure.set_menu_label(N_("_Fill with pattern seamless..."))
            procedure.set_attribution("Lloyd Konneker",
                                      "(c) GPL V3.0 or later",
                                      "2011")
            procedure.add_menu_path("<Image>/Edit/")

            procedure.add_argument_from_property(self, "pattern")

        return procedure

Gimp.main(ResynthFillPattern.__gtype__, sys.argv)

