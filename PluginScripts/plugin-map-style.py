#!/usr/bin/env python

'''
Gimp plugin.
Transfer style (color and surface texture) from a source image to the active, target image.

Requires resynthesizer plug-in.

Author:
lloyd konneker, lkk

Version:
1.0 lkk 7/15/2010 Initial version.  Released to Gimp Registry.
1.1 lkk 8/1/2010 Unreleased
1.2 lkk 8/10/2010

Change log:
_________________
1.1
  Bug: Fixed test of mode variable, since it is a string, needs explicit test for == 1
  Bug: Added remove Selection Mask copy channel in make_grayscale_map
1.2
  Changes for new resynthesizer: no need to synchronize, remove alphas
  Fixed improper adjustment of contrast of source: only adjust source map.

TODO
a quality setting that changes the parameters to resynth

License:

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

The GNU Public License is available at
http://www.gnu.org/copyleft/gpl.html


Users Guide
___________

What this plugin does:

Transfers artistic style from one image to another.  Often the source is an artistic image and the target is a realistic, photo image.  But you can also transfer between artistic images or between realistic images.

An artist might say this plugin "renders in the media and style from another image."  A computer user might say it "renders in the theme of another image."

Transferring style means transferring small scale features (color and texture) to an image while retaining large scale features (objects.)

Map can mean "transfer and transform".  This plugin gives limited control of the transform.  That is, colors are usually mapped to similar colors (hues.)  This plugin is not intended to do "false color" (but it might have that effect.)

Style can mean "color and surface."  Texture mapping usually means just surface (pattern of brightness, e.g. a weave or grain.)  This plugin can transfer both color and surface.

This plugin has more effect than just an overlay or screen or a map.  A screen usually applies a texture uniformly across an image.  This plugin transfers style in patches.  The style in a region can come from any patch of the source, or be synthesized (mixed) from many patches of the source.

The transfer is not exactly a copy, again because of optional synthesis or mixing.

About the selection:

Usually you transfer between separate images, the target and source images.  You can make a selection in either image, or both.  If there is no selection, the plugin uses the entire layer.

The target is the active LAYER and you can choose the source LAYER.  Note that the plugin doesn't use everything visible in an image, just one layer.

SPECIAL CASE: If the target and source layers are in the same image, the source style comes from the inverse of the selection in the source layer.  Similarly, if the target and source layers are the same layer, the target is the selection and the style comes from the inverse of the selection, i.e. outside the selection.  In this case, the effect is little if there is no difference in texture between the inside and outside of the selection, or a distort, if there is a difference.

About the settings:

"Percent transfer:" how much style to transfer.  Less transfer means the effect retains the large scale objects of the original, but gives the image a grainy surface.  More transfer means the effect leaves only a ghost of the large scale objects, and almost fully copies the style image (with less synthesis or mixing.)

"Map by:" whether color affects the style transfer, when both target and source are in color.  If you choose "color and brightness", style colors are more apt to be transferred to areas with same colors.  However, it is still possible that colors are radically transformed, if the surface (brightness pattern) is a better match.  If you choose "brightness only", style colors are more apt to be radically transformed.

This setting has less effect if there are no color matches between source and target (e.g. one is all red and the other is all green) or if the target image is GRAY.  This setting has NO effect if the source image or both images are GRAY.

About image modes:

You can transfer style between any combination of RGB and GRAY images.   The plugin changes the mode of the target to the mode of the source as necessary.

Why this plugin:

This plugin is a front-end to the separate resynthesizer plugin.  This plugin simplifies using the resynthesizer plugin.  It automates many steps.  It hides several complexities of the resynthesizer plugin:  selection, modes, alpha channels, and settings.



Programming notes:
_________________

IN: The active image and layer.
    The selection in the active image.
    The selection in any layers chosen for source.
OUT: The active image, altered.  The source is unaltered.
  Target mode can be altered, but with the implied consent of the user.

This plugin is mostly about UI and simplifications for user (the engine does the image processing):
making maps automatically
synchronization of alphas (note the new resynthesizer ignores alphas.)
synchronization of modes
abstracting the settings
contrast adjustment

'''

import sys
import math

import gi
gi.require_version('Gimp', '3.0')
from gi.repository import Gimp
gi.require_version('GimpUi', '3.0')
from gi.repository import GimpUi
from gi.repository import GObject
from gi.repository import GLib
from gi.repository import Gio
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

def N_(message): return message
def _(message): return GLib.dgettext(None, message)

def make_grayscale_map(image, drawable):
  '''
  Make a grayscale copy for a map.

  Maps must be same size as their parent image.

  If image is already grayscale, return it without copying.

  Maps don't need a selection, since the resynthesizer looks at parent drawables for the selection.
  '''
  if image.get_base_type() == Gimp.ImageBaseType.GRAY :
    return image, drawable

  # Save selection, copy entire image, and restore
  original_selection = Gimp.Selection.save(image)
  Gimp.Selection.all(image) # copy requires selection
  Gimp.edit_copy([drawable])
  if original_selection:
    image.select_item(Gimp.ChannelOps.REPLACE, original_selection) # restore selection in image
    image.remove_channel(original_selection) # cleanup the copied selection mask
    # !!! Note remove_channel not drawable_delete

  # Make a copy, greyscale
  temp_image = Gimp.edit_paste_as_new_image()
  temp_image.convert_grayscale()
  temp_drawable = temp_image.get_selected_drawables()
  return temp_image, temp_drawable[0]


def synchronize_modes(target_image, source_image) :
  '''
  User-friendliness:
  If mode of target is not equal to mode of source source, change modes.
  Resynthesizer requires target and source to be same mode.
  Assert target is RGB or GRAY (since is precondition of plugin.)
  UI decision: make this quiet, presume user intends mode change.
  But don't permanently change mode of source.
  Always upgrade GRAY to RGB, not downgrade RGB to GRAY.
  '''
  target_mode = target_image.get_base_type()
  source_mode = source_image.get_base_type()

  if target_mode != source_mode :
    if target_mode == GRAY:
      target_image.convert_rgb()
    else : # target is RGB and source is GRAY
      # Assert only convert a copy of source,
      # user NEVER intends original source be altered.
      source_image.convert_rgb()

def copy_selection_to_image(drawable) :
  '''
  If image has a selection, copy selection to new image, and prepare it for resynthesizer,
  else return a copy of the entire source image.
  This is called for the source image, where it helps performance to reduce size and flatten.
  '''
  image = drawable.get_image()

  # copy selection or whole image
  Gimp.edit_copy([drawable])
  image_copy = Gimp.edit_paste_as_new_image()
  # Activate layer, and remove alpha channel
  image_copy.flatten()
  layer_copy = image_copy.list_selected_layers()
  # In earlier version, futzed with selection to deal with transparencey
  #display_debug_image(image_copy)
  return image_copy, layer_copy[0]


def synchronize_contrast( drawable, source_drawable, percent_transfer) :
  '''
  Adjust contrast of source, to match target.
  Adjustment depends inversely on percent_transfer.
  Very crude histogram matching.
  '''
  # histogram upper half: typical mean is 191 (3/4*255). Skew of mean towards 255 means high contrast.
  result = Gimp.Drawable.histogram(drawable, Gimp.HistogramChannel.VALUE, 128, 255)
  mean = result[1]
  deviation = result[2]
  median = result[3]
  pixel = result[4]
  count = result[5]
  percentile = result[6]

  result = Gimp.Drawable.histogram(source_drawable, Gimp.HistogramChannel.VALUE, 128, 255)
  source_mean = result[1]
  source_deviation = result[2]
  source_median = result[3]
  pixels = result[4]
  count = result[5]
  percentile = result[6]
  # if mean > source_mean:  # target has more contrast than source
  # Adjust contrast of source.
  # Inversely proportional to percent transfer.
  # 2.5 is from experimentation with gimp_brightness_contrast which seems linear in its effect.
  contrast_control = (mean - source_mean) * 2.5 * (1 - (percent_transfer / 100))
  # clamp to valid range (above formula is lazy, ad hoc)
  if contrast_control < -127: contrast_control = -127
  if contrast_control > 127: contrast_control = 127
  Gimp.brightness_contrast(source_drawable, 0, contrast_control)

def calculate_map_weight(percent_transfer) :
  '''
  This is a GUI design discussion.
  Transform percent_transfer to map_weight parameter to resynthesizer.
  For resynthesizer:
  map weight 0 means copy source to target, meaning ALL style.
  map weight 0.5 means just a grainy transfer of style (as little as is possible.)
  Transform from a linear percent GUI, because user more comfortable than with a ratio [.5, 0]
  which is backwards to the usual *less on the left*.
  By experiment, a sinusoid gives good results for linearizing the non-linear map_weight control.
  '''
  return math.acos((percent_transfer/100)*2 -1)/(2*3.14)

def run(procedure, run_mode, image, n_layers, layers, args, data):

    progress_bar = None
    config = None

    if run_mode == Gimp.RunMode.INTERACTIVE:
        config = procedure.create_config()

        config.begin_run(image, run_mode, args)

        GimpUi.init("plugin-map-style.py")
        use_header_bar = Gtk.Settings.get_default().get_property("gtk-dialogs-use-header")
        dialog = GimpUi.Dialog(use_header_bar=use_header_bar,
                             title=_("Style"))
        dialog.add_button("_Cancel", Gtk.ResponseType.CANCEL)
        dialog.add_button("_OK", Gtk.ResponseType.OK)

        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL,
                       homogeneous=False, spacing=10)
        dialog.get_content_area().add(vbox)
        vbox.show()

        # Create grid to set all the properties inside.
        grid = Gtk.Grid()
        grid.set_column_homogeneous(False)
        grid.set_border_width(10)
        grid.set_column_spacing(10)
        grid.set_row_spacing(10)
        vbox.add(grid)
        grid.show()

        # Drawable combobox parameter
        label = Gtk.Label.new_with_mnemonic(_("_Source of style"))
        grid.attach(label, 0, 0, 1, 1)
        label.show()
        combo = GimpUi.DrawableComboBox.new (None, None, None)
        grid.attach(combo, 1, 0, 1, 1)
        combo.show()

        # Percent parameter
        label = Gtk.Label.new_with_mnemonic(_("_Percent Transfer"))
        grid.attach(label, 0, 1, 1, 1)
        label.show()
        spin = GimpUi.prop_spin_button_new(config, "percent_transfer", step_increment=1.0, page_increment=1.0, digits=0)
        grid.attach(spin, 1, 1, 1, 1)
        spin.show()

        # Map mode radio parameter
        label = Gtk.Label.new_with_mnemonic(_("_Map by"))
        grid.attach(label, 0, 2, 1, 1)
        label.show()
        map_combo = GimpUi.IntComboBox.new ([_("Color and brightness"), _("Brightness only")])
        if config.get_property("map_mode") != None:
          map_combo.set_active (config.get_property("map_mode"))
        else:
         map_combo.set_active (0)
        grid.attach(map_combo, 1, 2, 1, 1)
        map_combo.show()

        progress_bar = Gtk.ProgressBar()
        vbox.add(progress_bar)
        progress_bar.show()

        dialog.show()
        if not dialog.run():
            dialog.destroy()
            config.end_run(Gimp.PDBStatusType.CANCEL)
            return procedure.new_return_values(Gimp.PDBStatusType.CANCEL, GLib.Error())
        else:
            map_mode = map_combo.get_active ()
            id = (GimpUi.IntComboBox.get_active(combo))[1]
            if id != -1:
                source_drawable = Gimp.Drawable.get_by_id(id)
            else:
                dialog.destroy()
                config.end_run(Gimp.PDBStatusType.CANCEL)
                return procedure.new_return_values(Gimp.PDBStatusType.CANCEL, GLib.Error())
            dialog.destroy()

    if run_mode != Gimp.RunMode.INTERACTIVE:
        source_drawable = args.index (1)
        map_mode = args.index(2)

    percent_transfer = args.index(2)


    #ACTUAL ALGORITHM
    '''
    Main body of plugin to transfer style from one image to another.

    !!! Note map_mode is type string, "if map_mode:" will not work.
    '''
    Gimp.Image.undo_group_start (image)

    # Get image of source drawable
    source_image = Gimp.Item.get_image(source_drawable)

    '''
    User-friendliness.
    Note the drawable chooser widget in Pygimp does not allow us to prefilter INDEXED mode.
    So check here and give a warning.
    '''
    # These are the originals base types, and this plugin might change the base types
    original_source_base_type = source_image.get_base_type()
    original_target_base_type = image.get_base_type()

    if original_source_base_type == Gimp.ImageBaseType.INDEXED :
        Gimp.message(_("The style source cannot be of mode INDEXED"));
        return

    if image == source_image and layers[0] == source_drawable:
        is_source_copy = False
        '''
        If source is same as target,
        then the old resynthesizer required a selection (engine used inverse selection for corpus).
        New resynthesizer doesn't need a selection.
        If source same as target, effect is similar to a blur.
        '''
        # assert modes and alphas are same (since they are same layer!)
    else: # target layer is not the source layer (source could be a copy of target, but effect is none)
        # Copy source always, for performance, and for possible mode change.
        is_source_copy = True
        source_image, source_drawable = copy_selection_to_image(source_drawable)

        # Futz with modes if necessary.
        synchronize_modes(image, source_image)

        '''
        Old resythesizer required both images to have alpha, or neither.
        synchronize_alphas( drawable, source_drawable)
        '''

    '''
    TODO For performance, if there is a selection in target, it would be better to copy
    selection to a new layer, and later merge it back (since resynthesizer engine reads
    entire target into memory.  Low priority since rarely does user make a selection in target.
    '''

    '''
    !!! Note this plugin always sends maps to the resynthesizer,
    and the "percent transfer" setting is always effective.
    However, maps may not be separate,copied images unless converted to grayscale.
    '''

    # Copy and reduce maps to grayscale: at the option of the user
    # !!! Or if the target was GRAY and source is RGB, in which case maps give a better result.
    # Note that if the target was GRAY, we already upgraded it to RGB.
    if map_mode == 1 or (original_source_base_type == Gimp.ImageBaseType.RGB and original_target_base_type == Gimp.ImageBaseType.GRAY) :
        # Convert mode, but in new temp image and drawable
        target_map_image, target_map_drawable = make_grayscale_map(image, layers[0])
        source_map_image, source_map_drawable = make_grayscale_map(source_image, source_drawable)

        target_map = target_map_drawable
        source_map = source_map_drawable
        # later, delete temp images

        # User control: adjust contrast of source_map as a function of percent transfer
        # Hard to explain why, but experimentation shows result more like user expectation.
        # TODO This could be improved.
        # !!! Don't change the original source, only a temporary map we created
        synchronize_contrast(layers[0], source_map, percent_transfer)
    else :
        # !!! Maps ARE the target and source, not copies
        source_map = source_drawable
        target_map = layers[0]


    '''
    Parameters to resynthesizer:

    htile and vtile = 1 since it reduces artifacts around edge

    map_weight I linearize since easier on users than an exponential

    use_border = 1 since there might be a selection and context (outside target).

    9 neighbors (a 3x3 patch) and 200 tries for speed

    '''

    map_weight = calculate_map_weight(percent_transfer)


    # !!! This is for version of resynthesizer, with an uninverted selection
    positive = True
    border = 1
    nullLayers = None
    t2 = 0.117
    t3 = 9
    t4 = 200

    result = Gimp.get_pdb().run_procedure('plug-in-resynthesizer', [
        GObject.Value(Gimp.RunMode, Gimp.RunMode.NONINTERACTIVE),
        GObject.Value(Gimp.Image, image),
        GObject.Value(GObject.TYPE_INT, n_layers),
        GObject.Value(Gimp.ObjectArray, Gimp.ObjectArray.new(Gimp.Drawable, [layers[0]], False)),
        GObject.Value(GObject.TYPE_BOOLEAN, positive),
        GObject.Value(GObject.TYPE_BOOLEAN, positive),
        GObject.Value(GObject.TYPE_INT, border),
        GObject.Value(Gimp.Drawable, source_drawable),
        GObject.Value(Gimp.Drawable, source_map),
        GObject.Value(Gimp.Drawable, target_map),
        GObject.Value(GObject.TYPE_DOUBLE, map_weight),
        GObject.Value(GObject.TYPE_DOUBLE, t2),
        GObject.Value(GObject.TYPE_INT, t3),
        GObject.Value(GObject.TYPE_INT, t4),
    ])

  # Clean up.
  # Delete working images: separate map images and copy of source image
    if map_mode == 1:  # if made working map images
        Gimp.Image.delete(target_map_image)
        Gimp.Image.delete(source_map_image)
    if is_source_copy:  # if created a copy earlier
        Gimp.Image.delete(source_image)

    Gimp.Image.undo_group_end(image)

    # If the execution was successful, save parameters so they will be restored next time we show the dialog.
    #if result.index(0) == Gimp.PDBStatusType.SUCCESS and config is not None:
    config.end_run(Gimp.PDBStatusType.SUCCESS)

    return procedure.new_return_values(Gimp.PDBStatusType.SUCCESS, GLib.Error())

class MapStyle(Gimp.PlugIn):

    ## Parameters ##
    __gproperties__ = {
        "source_drawable":  (Gimp.Drawable,
                             _("_Source of style"),
                             "Source of style",
                             GObject.ParamFlags.READWRITE),
        "percent_transfer":  (float,
                              _("_Percent transfer"),
                              "Percent transfer",
                              10.0, 90.0, 10.0,
                              GObject.ParamFlags.READWRITE),
        "map_mode":  (int,
                      _("_Map by"),
                      "Map by",
                      0, 1, 0,
                      GObject.ParamFlags.READWRITE),
    }

    ## GimpPlugIn virtual methods ##
    def do_set_i18n(self, procname):
        return True, 'gimp30-python', None

    def do_query_procedures(self):
        return ['map-style']

    def do_create_procedure(self, name):
        procedure = None
        if name == 'map-style':
            procedure = Gimp.ImageProcedure.new(self, name,
                                                Gimp.PDBProcType.PLUGIN,
                                                run, None)

            procedure.set_image_types("RGB*, GRAY*")
            procedure.set_documentation (
                _("Create a new image with texture from the current image or selection. Optionally, create image edges suited for further, seamless tiling."),
                globals()["__doc__"],  # This includes the docstring, on the top of the file
                name)
            procedure.set_menu_label(_("_Style..."))
            procedure.set_attribution("Lloyd Konneker",
                                      "(c) GPL V3.0 or later",
                                      "2010")
            procedure.add_menu_path("<Image>/Filters/Map/")

            procedure.add_argument_from_property(self, "source_drawable")
            procedure.add_argument_from_property(self, "percent_transfer")
            procedure.add_argument_from_property(self, "map_mode")

        return procedure

Gimp.main(MapStyle.__gtype__, sys.argv)
