#!/usr/bin/env python

'''
Gimp plugin.

Create new image having texture synthesized from the selection.
Works best if selection is natural (fractal).
Can work with man-made regular texture.
Works worst with man-made, structured but not regular, symbols.
Sometimes called rendering a texture.

Requires resynthesizer plug-in.

Author:
lloyd konneker, lkk, bootch at nc.rr.com

Version:
1.0 lkk 7/15/2010 Initial version
1.1 lkk 4/10/2011 Fixed a bug with layer types impacting greyscale images.

License:

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

The GNU Public License is available at
http://www.gnu.org/copyleft/gpl.html


The effect for users:
Similar to "Fill resynthesized pattern" except:
  - here the arguments are reversed: you select a texture and create a new image
    instead of selecting an area and choosing a pattern.
  - here the result is less random (since Fill resynthesized adds noise.)
Different from tiling since:
  - seamless and irregular pattern

The continuum of randomness versus speed:
  - Filters.Map.Tile is fast but has seams and regular pattern (even if you use "Make Seamless" first.)
  - Filter.Render.Texture a tile followed by tiling is seamless but still has regularity.
  - Filte.Render.Texture an entire image is slower but seamless and moderately irregular.
  - Edit.Fill with resynthesized pattern is slowest but seamless and highly irregular, unpatterned.

This filter is not tiling (instead resynthesizing) but makes
an image that you can then use to tile with especially if
you choose the option to make the edges suitable for tiling.

IN: The selection (or the entire active drawable) is the source of texture and is not changed.
OUT New image, possibly resized canvas, same scale and resolution.

TODO a quality setting
'''

import sys

import gi
gi.require_version('Gimp', '3.0')
from gi.repository import Gimp
gi.require_version('GimpUi', '3.0')
from gi.repository import GimpUi
from gi.repository import GObject
from gi.repository import GLib
from gi.repository import Gio
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

def N_(message): return message
def _(message): return GLib.dgettext(None, message)

def new_resized_image(image, resize_ratio):
  # Create new image resized by a ratio from *selection* in the old image

  result = Gimp.Selection.bounds(image)
  is_selection = result[1]
  ulx = result[2]
  uly = result[3]
  lrx = result[4]
  lry = result[5]

  new_width = 0
  new_height = 0

  if not is_selection :
    # Resynthesizer will use the entire source image as corpus.
    # Resize new image in proportion to entire source image.
    new_width = int(image.get_width() * resize_ratio)
    new_height = int(image.get_height() * resize_ratio)
  else :
    # Resize new image in proportion to selection in source
    new_width = int((lrx - ulx) * resize_ratio)
    new_height = int((lry - uly) * resize_ratio)

  new_basetype = image.get_base_type() # same as source
  active_layer = (image.list_selected_layers())[0]
  new_layertype = active_layer.type()
  new_image = Gimp.Image.new(new_width, new_height, new_basetype)
  # !!! Note that gimp_layer_new wants a layer type, not an image basetype
  new_drawable = Gimp.Layer.new(new_image, "Texture", new_width, new_height,
    new_layertype, 100, Gimp.LayerMode.NORMAL)
  # The new layer is opaque, but the new image has transparent pixels (for case RGBA)
  # Resynthesizer will not change the transparency, so make pixels opaque.
  # Fill with white will make them opaque.
  new_drawable.fill(Gimp.FillType.WHITE)
  # A new layer must be added to the image.
  new_image.insert_layer(new_drawable, None, 0)
  return new_image, new_drawable

def display_image(image):
  Gimp.Display.new(image)
  Gimp.displays_flush()

def run(procedure, run_mode, image, n_layers, layers, args, data):

    progress_bar = None
    config = None

    if run_mode == Gimp.RunMode.INTERACTIVE:

        config = procedure.create_config()

        config.begin_run(image, run_mode, args)

        GimpUi.init("plugin-render-texture.py")
        dialog = GimpUi.ProcedureDialog(procedure=procedure, config=config)
        dialog.fill (None)

        if not dialog.run():
            dialog.destroy()
            config.end_run(Gimp.PDBStatusType.CANCEL)
            return procedure.new_return_values(Gimp.PDBStatusType.CANCEL, GLib.Error())
        else:
            dialog.destroy()

    resize_ratio = args.index(0)
    make_tile = args.index(1)

    #ACTUAL ALGORITHM
    '''
    Create a randomized texture image from the selection.
    The image can be suited for further, seamless tiling.
    The image is same scale and resolution but resized from the selection.
    Not undoable, no changes to the source (you can just delete the new image.)

    A selection in the source image is optional.
    If there is no selection, the resynthesizer will use the entire source image.
    '''

    # Its all or nothing, user must delete new image if not happy.
    image.undo_disable()

    '''
    Create new image, optionally resized, and display for it.
    '''
    result = new_resized_image(image, resize_ratio)
    new_image = result[0]
    new_drawable = result[1]

    new_image.undo_disable()
    if not new_drawable:
        raise RuntimeError("Failed create layer")

    '''
    copy original into temp and crop it to the selection to save memory in resynthesizer
    '''
    temp_image = Gimp.Image.duplicate(image)
    if not temp_image:
      raise RuntimeError("Failed duplicate image")

    # Get bounds, offset of selection
    result = Gimp.Selection.bounds(image)
    is_selection = result[1]
    ulx = result[2]
    uly = result[3]
    lrx = result[4]
    lry = result[5]
    if not is_selection :
        # No need to crop.  Resynthesizer will use all if no selection.
        pass
    else :
       temp_image.crop(lrx - ulx, lry - uly, ulx, uly)

    # Don't flatten because it turns transparency to background (white usually)
    work_layer = temp_image.list_selected_layers()
    if not work_layer:
        raise RuntimeError("Failed get active layer")

    # Insure the selection is all (not necessary, resynthesizer will use all if no selection.)
    Gimp.Selection.all(temp_image)

    # Settings for making edges suitable for seamless tiling afterwards.
    # That is what these settings mean in the resynthesizer:
    # wrap context probes in the target so that edges of target will be suitable for seamless tiling.
    # I.E. treat the target as a sphere when matching.
    if make_tile :
        htile = True
        vtile = True
    else :
        htile = False
        vtile = False

    # Call resynthesizer
    # use_border is moot since there is no context (outside the target) in the newImage.
    # The target is the entire new image, the source is the cropped copy of the selection.
    #
    # 9 neighbors (a 3x3 patch) and 200 tries for speed, since new image is probably large
    # and source is probably natural (fractal), where quality is not important.

    # For version of resynthesizer with uninverted selection
    negative = False
    border = 0
    nullLayers = None
    t1 = 0
    t2 = 0.117
    t3 = 9
    t4 = 200

    result = Gimp.get_pdb().run_procedure('plug-in-resynthesizer', [
        GObject.Value(Gimp.RunMode, Gimp.RunMode.NONINTERACTIVE),
        GObject.Value(Gimp.Image, new_image),
        GObject.Value(GObject.TYPE_INT, n_layers),
        GObject.Value(Gimp.ObjectArray, Gimp.ObjectArray.new(Gimp.Drawable, [new_drawable], False)),
        GObject.Value(GObject.TYPE_BOOLEAN, htile),
        GObject.Value(GObject.TYPE_BOOLEAN, vtile),
        GObject.Value(GObject.TYPE_INT, border),
        GObject.Value(Gimp.Drawable, work_layer[0]),
        GObject.Value(Gimp.Drawable, nullLayers),
        GObject.Value(Gimp.Drawable, nullLayers),
        GObject.Value(GObject.TYPE_DOUBLE, t1),
        GObject.Value(GObject.TYPE_DOUBLE, t2),
        GObject.Value(GObject.TYPE_INT, t3),
        GObject.Value(GObject.TYPE_INT, t4),
    ])

    display_image(new_image)

    Gimp.Image.delete(temp_image)
    image.undo_enable()
    new_image.undo_enable()

    # If the execution was successful, save parameters so they will be restored next time we show the dialog.
    #if result.index(0) == Gimp.PDBStatusType.SUCCESS and config is not None:
    config.end_run(Gimp.PDBStatusType.SUCCESS)

    return procedure.new_return_values(Gimp.PDBStatusType.SUCCESS, GLib.Error())

class RenderTexture(Gimp.PlugIn):

    ## Parameters ##
    __gproperties__ = {
        "resize_ratio":  (float,
                          _("Ratio of size of _new image to source selection"),
                          "Ratio of size of new image to source selection",
                          0.5, 10.0, 2.0,
                          GObject.ParamFlags.READWRITE),
        "make_tile": (bool,
                       _("Make new image edges suitable for seamless _tiling"),
                       _("Make new image edges suitable for seamless tiling"),
                       False,
                       GObject.ParamFlags.READWRITE),
    }

    ## GimpPlugIn virtual methods ##
    def do_set_i18n(self, procname):
        return True, 'gimp30-python', None

    def do_query_procedures(self):
        return ['render-texture']

    def do_create_procedure(self, name):
        procedure = None
        if name == 'render-texture':
            procedure = Gimp.ImageProcedure.new(self, name,
                                                Gimp.PDBProcType.PLUGIN,
                                                run, None)

            procedure.set_image_types("RGB*, GRAY*")
            procedure.set_documentation (
                _("Create a new image with texture from the current image or selection. Optionally, create image edges suited for further, seamless tiling."),
                globals()["__doc__"],  # This includes the docstring, on the top of the file
                name)
            procedure.set_menu_label(_("_Texture..."))
            procedure.set_attribution("Lloyd Konneker",
                                      "(c) GPL V3.0 or later",
                                      "2009")
            procedure.add_menu_path("<Image>/Filters/Render/")

            procedure.add_argument_from_property(self, "resize_ratio")
            procedure.add_argument_from_property(self, "make_tile")

        return procedure

Gimp.main(RenderTexture.__gtype__, sys.argv)
